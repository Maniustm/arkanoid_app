package com.example.arkanoid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainGamePanel extends SurfaceView implements
		SurfaceHolder.Callback {

	private static final String TAG = MainGamePanel.class.getSimpleName();

	private MainThread thread;
	private Pileczka pileczka;
	private Paletka paletka;
	private DisplayMetrics displaymetrics;
	private Klocek klocek;
	private Klocek[][] tablicaKlockow = new Klocek[5][5];
	private Bitmap bitmapPaletka;
	private Bitmap bitmapKlocek;
	private Bitmap bitmapPileczka;
	private int iloscZbitych;

	public MainGamePanel(Context context) {
		super(context);
		// adding the callback (this) to the surface holder to intercept events
		getHolder().addCallback(this);

		// create bad and load bitmap
		displaymetrics = getResources().getDisplayMetrics();
		bitmapPaletka = BitmapFactory.decodeResource(getResources(),
				R.drawable.paletka);
		paletka = new Paletka(bitmapPaletka,
				(int) (displaymetrics.widthPixels * 0.5),
				(int) (displaymetrics.heightPixels * 0.75));

		// create ball and load bitmap
		bitmapPileczka = BitmapFactory.decodeResource(getResources(),
				R.drawable.pileczka);
		pileczka = new Pileczka(bitmapPileczka, paletka.getX(), paletka.getY()
				+ bitmapPaletka.getHeight() / 2);

		bitmapKlocek = BitmapFactory.decodeResource(getResources(),
				R.drawable.klocek);

		int x = 30;
		int y = 30;

		for (int i = 0; i < tablicaKlockow.length; i++) {

			if (i > 0) {
				y = tablicaKlockow[i - 1][0].getY() + bitmapKlocek.getHeight()
						/ 2 + 50;
				x = 30;
			}

			for (int j = 0; j < tablicaKlockow[i].length; j++) {

				klocek = new Klocek(bitmapKlocek, x, y);
				tablicaKlockow[i][j] = klocek;
				x = tablicaKlockow[i][j].getX() + bitmapKlocek.getWidth() / 2
						+ 50;
			}
		}

		// create the game loop thread
		thread = new MainThread(getHolder(), this);

		// make the GamePanel focusable so it can handle events
		setFocusable(true);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// at this point the surface is created and
		// we can safely start the game loop
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "Surface is being destroyed");
		// tell the thread to shut down and wait for it to finish
		// this is a clean shutdown
		boolean retry = true;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// try again shutting down the thread
			}
		}
		Log.d(TAG, "Thread was shut down cleanly");
	}

	public void render(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
	
		iloscZbitych = 0;
		
		for (int i = 0; i < tablicaKlockow.length; i++) {

			for (int j = 0; j < tablicaKlockow[i].length; j++) {
				
				if (tablicaKlockow[i][j].isZbity()) {
					iloscZbitych += 1;
				}
			}
		}
		
		if ((pileczka.getY() >= getHeight()) && (iloscZbitych != tablicaKlockow.length * tablicaKlockow.length) /*paletka.getY() + bitmapPaletka.getHeight() / 2*/) {
			 
		Bitmap bitmapPrzegrana = BitmapFactory.decodeResource(
				getResources(), R.drawable.przegrana);
		canvas.drawBitmap(bitmapPrzegrana,150,300, null);

	} else if(iloscZbitych == tablicaKlockow.length * tablicaKlockow.length){
		
		Bitmap bitmapWygrana = BitmapFactory.decodeResource(
				getResources(), R.drawable.wygrana);
		canvas.drawBitmap(bitmapWygrana,120,300, null);
		
	}else
		{
			pileczka.draw(canvas);
			paletka.draw(canvas);
			klocek.draw(canvas, tablicaKlockow);
		}
	}

	public void update() {

		// check collision with right wall if heading right
		if (pileczka.getSpeed().getxDirection() == Speed.DIRECTION_RIGHT
				&& pileczka.getX() + pileczka.getBitmap().getWidth() / 2 >= getWidth()) {
			pileczka.getSpeed().toggleXDirection();
		}
		// check collision with left wall if heading left
		if (pileczka.getSpeed().getxDirection() == Speed.DIRECTION_LEFT
				&& pileczka.getX() - pileczka.getBitmap().getWidth() / 2 <= 0) {
			pileczka.getSpeed().toggleXDirection();
		}
		// check collision with bottom wall if heading down

		/*
		 * if (pileczka.getSpeed().getyDirection() == Speed.DIRECTION_DOWN &&
		 * pileczka.getY() + pileczka.getBitmap().getHeight() / 2 >=
		 * getHeight()) { pileczka.getSpeed().toggleYDirection(); }
		 */

		if (pileczka.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
				&& ((pileczka.getY() >= paletka.getY()
						- bitmapPaletka.getHeight() / 2) && (pileczka.getY() <= paletka
						.getY() + bitmapPaletka.getHeight() / 2))
				&& (pileczka.getX() >= paletka.getX()
						- bitmapPaletka.getWidth() / 2)
				&& pileczka.getX() <= paletka.getX() + bitmapPaletka.getWidth()
						/ 2) {
			pileczka.getSpeed().toggleYDirection();
		}

		for (int i = 0; i < tablicaKlockow.length; i++) {

			for (int j = 0; j < tablicaKlockow[i].length; j++) {

				if (((pileczka.getY() - bitmapPileczka.getHeight() / 2 >= tablicaKlockow[i][j]
						.getY() + bitmapKlocek.getHeight() / 2) && (pileczka
						.getY() - bitmapPileczka.getHeight() / 2 <= tablicaKlockow[i][j]
						.getY() - bitmapKlocek.getHeight() / 2))
						&& (pileczka.getX() >= tablicaKlockow[i][j].getX()
								- bitmapKlocek.getWidth() / 2)
						&& (pileczka.getX() <= tablicaKlockow[i][j].getX()
								+ bitmapKlocek.getWidth() / 2)) {

					if (tablicaKlockow[i][j].isZbity() != true) {
						pileczka.getSpeed().toggleYDirection();
					}
					tablicaKlockow[i][j].setZbity(true);
					
				}

				if (((pileczka.getY() + bitmapPileczka.getHeight() / 2 >= tablicaKlockow[i][j]
						.getY() - bitmapKlocek.getHeight() / 2) && (pileczka
						.getY() + bitmapPileczka.getHeight() / 2 <= tablicaKlockow[i][j]
						.getY() + bitmapKlocek.getHeight() / 2))
						&& (pileczka.getX() >= tablicaKlockow[i][j].getX()
								- bitmapKlocek.getWidth() / 2)
						&& (pileczka.getX() <= tablicaKlockow[i][j].getX()
								+ bitmapKlocek.getWidth() / 2)) {

					if (tablicaKlockow[i][j].isZbity() != true) {
						pileczka.getSpeed().toggleYDirection();
					}
					tablicaKlockow[i][j].setZbity(true);
					
				}

				if (((pileczka.getX() + bitmapPileczka.getWidth() / 2 >= tablicaKlockow[i][j]
						.getX() - bitmapKlocek.getWidth() / 2) && (pileczka
						.getX() + bitmapPileczka.getWidth() / 2 <= tablicaKlockow[i][j]
						.getX() + bitmapKlocek.getWidth() / 2))
						&& (pileczka.getY() >= tablicaKlockow[i][j].getY()
								- bitmapKlocek.getHeight() / 2)
						&& (pileczka.getY() <= tablicaKlockow[i][j].getY()
								+ bitmapKlocek.getHeight() / 2)) {

					if (tablicaKlockow[i][j].isZbity() != true) {
						pileczka.getSpeed().toggleXDirection();
					}
					tablicaKlockow[i][j].setZbity(true);
					
				}

				if (((pileczka.getX() - bitmapPileczka.getWidth() / 2 >= tablicaKlockow[i][j]
						.getX() - bitmapKlocek.getWidth() / 2) && (pileczka
						.getX() - bitmapPileczka.getWidth() / 2 <= tablicaKlockow[i][j]
						.getX() + bitmapKlocek.getWidth() / 2))
						&& (pileczka.getY() >= tablicaKlockow[i][j].getY()
								- bitmapKlocek.getHeight() / 2)
						&& (pileczka.getY() <= tablicaKlockow[i][j].getY()
								+ bitmapKlocek.getHeight() / 2)) {

					if (tablicaKlockow[i][j].isZbity() != true) {
						pileczka.getSpeed().toggleXDirection();
					}
					tablicaKlockow[i][j].setZbity(true);
					
				}

			}

		}

		// check collision with top wall if heading up
		if (pileczka.getSpeed().getyDirection() == Speed.DIRECTION_UP
				&& pileczka.getY() - pileczka.getBitmap().getHeight() / 2 <= 0) {
			pileczka.getSpeed().toggleYDirection();
		}
		// Update the lone ball

			pileczka.update();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// delegating event handling to the bad
			paletka.handleActionDown((int) event.getX(), (int) event.getY());
		}
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			// the gestures
			if (paletka.isTouched()) {
				// the bad was picked up and is being dragged
				paletka.setX((int) event.getX());
				paletka.setY((int) (displaymetrics.heightPixels * 0.75));
			}
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			// touch was released
			if (paletka.isTouched()) {
				paletka.setTouched(false);
			}
		}
		return true;
	}

}
