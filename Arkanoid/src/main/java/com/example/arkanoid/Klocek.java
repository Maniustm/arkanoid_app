package com.example.arkanoid;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Klocek {

	private Bitmap bitmap; // the actual bitmap
	private int x;
	private int y;
	private boolean zbity;

	public Klocek(Bitmap bitmap, int x, int y) {

		this.bitmap = bitmap;
		this.x = x;
		this.y = y;
		this.zbity = false;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public boolean isZbity() {
		return zbity;
	}

	public void setZbity(boolean zbity) {
		this.zbity = zbity;
	}

	public void draw(Canvas canvas, Klocek[][] tab) {

		for (int i = 0; i < tab.length; i++) {

			for (int j = 0; j < tab[i].length; j++) {
				if (!tab[i][j].zbity) {
					canvas.drawBitmap(bitmap, tab[i][j].getX(),
							tab[i][j].getY(), null);
				}
			}
		}
	}

}
